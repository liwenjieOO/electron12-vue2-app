import { app,remote, dialog } from 'electron'
export const handleOpenDirectory = async () => {
  const options = {
    title: '选择目录',
    properties: ['openDirectory']
  };
  const { canceled, filePaths } = await dialog.showOpenDialog(options)
  if (canceled) {
    return
  } else {
    return filePaths[0]
  }
}
export const handleOpenFile = async () => {
  const options = {
    title: '选择程序',
    properties: ['openFile']
  };
  const { canceled, filePaths } = await dialog.showOpenDialog(options)
  if (canceled) {
    return
  } else {
    return filePaths[0]
  }
}
export const getAppPath = () => {
  const APP = process.type === 'renderer' ? remote.app : app
  const STORE_PATH = APP.getPath('userData')
  return STORE_PATH
}
