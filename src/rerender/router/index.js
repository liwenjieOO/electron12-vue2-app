import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/layouts'

Vue.use(VueRouter)

export const constantRoutes = [
    {
        path: '/',
        component: Layout,
        redirect: '/index',
        children: [
            {
                path: 'index',
                name: 'Index',
                component: () => import('@/views/index/index'),
                meta: {
                    title: '首页',
                    icon: 'home',
                    affix: true,
                },
            },
        ],
    },
    {
        path:'/setting',
        component:()=>import('@/views/setting/index'),
        meta:{
            title:'设置'
        }
    },
    {
        path: '/win',
        component: () => import('@/views/win/index'),
        redirect: '/win/a',
        children: [
            {
                path: 'a',
                name: 'aIndex',
                component: () => import('@/views/win/a')
            },
            {
                path: 'b',
                name: 'bIndex',
                component: () => import('@/views/win/b')
            },
        ],
    },
    {
        path: '*',
        component: Layout,
        redirect: '/404',
        children: [
            {
                path: '404',
                name: '404',
                component: () => import('@/views/404')
            },
        ],
    },
]

const router = new VueRouter({
    mode: 'hash',
    scrollBehavior: () => ({
        y: 0,
    }),
    routes: [
        ...constantRoutes,
    ],
})

export function resetRouter() {
    location.reload()
}

export default router