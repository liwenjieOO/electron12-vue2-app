import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import VsToast from '@vuesimple/vs-toast';

import '@/static/css/bootstrap.css';

Vue.config.productionTip = false
Vue.prototype.$toast = VsToast;
new Vue({
  el: '#app',
  router,
  store,
  render: (h) => h(App),
})
