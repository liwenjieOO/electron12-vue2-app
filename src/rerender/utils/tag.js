import {
    dataInit,
    createOne,
    removeById,
    getAll,
} from '@/lowdb'

let db = dataInit('data/tag.json');

db.defaults({tags: []}).write();

export const createTag = async (tag) => {
    console.log('create tag',tag)
    await createOne(db, 'tags', tag)
}

export const removeTag = async (tag) => {
    let { id } = tag
    await removeById(db, 'tags', id)
}
export const getTags = async () => {
    let res = await getAll(db,'tags')
    return res;
}





