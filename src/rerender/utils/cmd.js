import cmd from 'node-cmd';
import {
    dataInit,
    createOne,
    removeById,
    updateOne,
    getById,
    getByPagination,
    getMatch
} from '@/lowdb'

let db = dataInit('data/cmd.json');

db.defaults({cmds: []}).write();

export const runCmd = (path) => {
    cmd.run(`start ${path}`)
}

export const createCmd = async (cmd) => {
    await createOne(db, 'cmds', cmd)
}

export const removeCmd = async (cmd) => {
    let { id } = cmd
    await removeById(db, 'cmds', id)
}

export const updateCmd = async (cmd) => {
    await updateOne(db, 'cmds', cmd)
}

export const getCmd = async (cmd) => {
    let { id } = cmd
    let res = await getById(db,'cmds', id)
    return res;
}

export const getCmds = async (pagination) => {
    // let res = await getAll(db,'cmds',data)
    // return res;
    let res = await getByPagination(db,'cmds',pagination)
    return res;
}

export const getMatchCmds = async (pagination={},whereAttrs={}) => {
    // let res = await getAll(db,'cmds',data)
    // return res;
    let res = await getMatch(db,'cmds',pagination,whereAttrs)
    return res;
}




