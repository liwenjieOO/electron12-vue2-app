const state = () => ({
    tags: []
})
const getters = {
    tags: (state) => state.tags
};
const mutations = {
    SET_TAGS(state, tags) {
        state.tags = tags
    }
};
const actions = {
    setTags({ commit }, tags) {
        commit('SET_TAGS', tags)
    }
};

export default { state, getters, mutations, actions }