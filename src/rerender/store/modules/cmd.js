const state = () => ({
    cmds: [],
})
const getters = {
    cmds: (state) => state.cmds
};
const mutations = {
    CMDS(state, data) {
        state.cmds = data
    }
};
const actions = {
    setCmds({ commit }, data) {
        commit('CMDS', data)
    }
};

export default { state, getters, mutations, actions }