import { app } from '@/cloudbase/init';
export const getTodo = async (data) => {
    const res = await app.callFunction({
        name: "getTodo",
        data
    })
    return res.result
}

export const createTodo = async (data) => {
    const res = await app.callFunction({
        name: "createTodo",
        data
    })
    return res.result
}