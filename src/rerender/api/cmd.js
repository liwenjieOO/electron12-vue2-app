import request from '@/utils/request'

export function getCmds(params) {
  return request({
    url: '/cmd',
    method: 'get',
    params
  })
}
export function createCmd(data) {
  return request({
    url: '/cmd',
    method: 'post',
    data
  })
}

export function getCmd(params) {
  return request({
    url: '/cmd/item',
    method: 'get',
    params
  })
}


export function updateCmd(data) {
  return request({
    url: '/cmd/update',
    method: 'post',
    data
  })
}
export function removeCmd(data) {
  return request({
    url: '/cmd/remove',
    method: 'post',
    data
  })
}
