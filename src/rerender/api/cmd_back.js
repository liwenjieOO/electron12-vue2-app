import { app } from '@/cloudbase/init';
export const getCmd = async (data) => {
    const res = await app.callFunction({
        name: "getCmd",
        data
    })
    return res.result
}
export const getCmds = async (data) => {
    const res = await app.callFunction({
        name: "getCmds",
        data
    })
    return res.result
}

export const createCmd = async (data) => {
    const res = await app.callFunction({
        name: "createCmd",
        data
    })
    return res.result
}

export const removeCmd = async (data) => {
    const res = await app.callFunction({
        name: "removeCmd",
        data
    })
    return res.result
}

export const updateCmd = async (data) => {
    const res = await app.callFunction({
        name: "updateCmd",
        data
    })
    return res.result
}