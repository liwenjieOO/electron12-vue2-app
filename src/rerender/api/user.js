import request from '@/utils/request'

export function register(data) {
  return request({
    url: '/user/register',
    method: 'post',
    data,
  })
}

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data,
  })
}
export function updateUser(data){
  return request({
    url:'/user/update',
    method:'post',
    data
  })
}

export function deleteUser(data){
  return request({
    url:'/user/delete',
    method:'post',
    data
  })
}