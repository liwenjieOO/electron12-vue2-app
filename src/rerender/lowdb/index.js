
import fs from 'fs'
import path from 'path'
import { app, remote } from 'electron'
import moment from 'moment'
import LodashId from 'lodash-id'
const low = require('lowdb')
import FileSync from 'lowdb/adapters/FileSync'
import { v4 as uuid } from "uuid";
import { mkdir } from '@/utils/path'

export const dataInit =  (file) => {
    let STORE_PATH = getAppPath();
    const filePath = path.join(STORE_PATH, file);
    if (fs.existsSync(filePath)) {
        console.log('该路径已存在');
    } else {
        console.log('该路径不存在');
        mkdir(filePath);
    }
    const db =  lowdbInit(filePath)
    return db;
}

export const getAppPath = () => {

    const APP = process.type === 'renderer' ? remote.app : app
    const STORE_PATH = APP.getPath('userData')

    console.log('STORE_PATH--', STORE_PATH)
    return STORE_PATH
}

export const lowdbInit = (filePath) => {
    const adapter = new FileSync(filePath)
    const db = low(adapter)
    db._.mixin(LodashId)
    return db;
}
/**
 * 创建单个
*/
export const createOne = async (db, table, data) => {
    await db.read();
    let collection = db.get(table);
    let total = collection.size().value();
    let obj = Object.assign(data, { value: total, id: uuid(), created: moment().format("YYYY-MM-DD HH:mm:ss") })
    await collection.insert(obj).write();
}
/**
 * 删除单个
*/
export const removeById = async (db, table, id) => {
    await db.read();
    await db.get(table)
        .removeById(id)
        .write()
}
/**
 * 更新单个
*/
export const updateOne = async (db, table, data) => {
    let { id } = data;
    await db.read();
    let obj = Object.assign(data, { updated: moment().format("YYYY-MM-DD HH:mm:ss") })
    console.log('update', data, obj)
    await db.get(table).updateById(id, obj).write();
}
/**
 * 获取单个
*/
export const getById = async (db, table, id) => {
    await db.read();
    return await db.get(table).getById(id).value();
}

export const getAll = async (db, table) => {
    await db.read();
    const result = db.get(table).cloneDeep().value()
    console.log('lowdb tags',result)
    return result;
}

/**
 * 模糊查询获取全部
 *  {
     sortBy: "title",
     descending: true,//倒序
     size: 10,
     page: 1
   }
*/
export const getByPagination = async (db, table, pagination) => {
    let size = 10;
    let page = 1;
    let filter = {};
    if (pagination.sortBy) {
        filter.sortBy = pagination.sortBy;
    }
    if (pagination.descending) {
        filter.descending = pagination.descending
    }
    if (pagination.size) {
        size = pagination.size;
        filter.size = size
    }
    if (pagination.page) {
        page = pagination.page;
        filter.page = page
    }

    await db.read();
    const collection = db.get(table)
    const total = collection.size().value()
    const result = collection
        // .filter()
        .orderBy(filter.sortBy, filter.descending ? 'desc' : 'asc')
        .chunk(filter.size === -1 ? total : filter.size)
        .take(filter.page)
        .last()
        .value()
    return result;
}
export const getMatch = async (db, table, pagination, whereAttrs) => {
    let size = 10;
    let page = 1;
    let filter = {};
    if (pagination.sortBy) {
        filter.sortBy = pagination.sortBy;
    }
    if (pagination.descending) {
        filter.descending = pagination.descending
    }
    if (pagination.size) {
        size = pagination.size;
        filter.size = size
    }
    if (pagination.page) {
        page = pagination.page;
        filter.page = page
    }
    await db.read();
   
    const collection = db.get(table)
    const total = collection.size().value()

    console.log('whereAttrs',whereAttrs,44)
    const result = collection
        .filter(o => o.title.match(...whereAttrs))
        .orderBy(filter.sortBy, filter.descending ? 'desc' : 'asc')
        .chunk(filter.size === -1 ? total : filter.size)
        .take(filter.page)
        .last()
        .value()
    return result;
}