/**
 * res 的数据模型
 */

// 基础模块
class BaseRes {
    constructor({code, data, msg}) {
      this.code = code
      if (data) {
        this.data = data
      }
      if (msg) {
        this.msg = msg
      }
    }
  }
  
  
  // 成功的数据模型
  class SuccessRes extends BaseRes {
    constructor (data = {}) {
      super({
        code: 200,
        data,
        msg:'请求成功'
      })
    }
  }
  
  // 失败的数据模型
  class ErrorRes extends BaseRes {
    constructor ({ code, msg }) {
      super({
        code,
        msg
      })
    }
  }
  
  module.exports = {
    SuccessRes,
    ErrorRes
  }