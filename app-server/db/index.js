const mongoose = require('mongoose')
const config = require('../config/index')
module.exports = () => {
    let url = config.mongodb_url;
    mongoose.connect(url)
    mongoose.connection.once("open", function () {
        console.log("数据库连接成功！")
    })
}