const moment = require('moment');
const { Schema, model } = require('mongoose')
let TagSchema = new Schema({
    title: String,
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    created: {
        type: String,
        default: () => moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    },
    updated: {
        type: String,
        default: () => moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    },
})
TagSchema.pre('save', function (next) {
    const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    this.created = now
    next();
});
module.exports = model('Tag', TagSchema)