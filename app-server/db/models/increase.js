const mongoose = require('mongoose');
let IncreaseSchema = new mongoose.Schema({
    id: Number,
    sequence_value:{type:Number,default:0}
})

module.exports = mongoose.model('Increase', IncreaseSchema)