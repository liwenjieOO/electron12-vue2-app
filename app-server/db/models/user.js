const moment = require('moment')
const mongoose = require('mongoose');
let UserSchema = new mongoose.Schema({
    uid: Number,
    username: String,
    role: {
        type: String,
        default: 'user'
    },
    phone: String,
    password: String,
    gender: String,
    age: Number,
    avatar: String,
    content: String, //个人介绍
    created: String,
    updated: String
})
UserSchema.pre('save', function (next) {
    const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    this.created = now
    next();
});

UserSchema.pre('updateOne', function (next) {
    const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    this.set({ updated: now });
    next();
});
UserSchema.methods.toAuthJSON = function () {
    return {
        username: this.username,
        phone: this.phoen,
        gender: this.gender,
        age: this.age,
        content: this.content,
        avatar: this.avatar || 'http://localhost:3000/images/logo.png'
    };
};
module.exports = mongoose.model('User', UserSchema)