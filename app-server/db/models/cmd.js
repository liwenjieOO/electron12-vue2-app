const mongoose = require('mongoose');
const moment = require('moment');
let CmdSchema = new mongoose.Schema({
    title: String,
    path:String,
    category:String, //个人介绍
    created: {
        type: String,
        default: () => moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    },
    updated: {
        type: String,
        default: () => moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    },
})

module.exports = mongoose.model('Cmd', CmdSchema)