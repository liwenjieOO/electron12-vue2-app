module.exports = {
    env: 'development', //环境名称
    port: 3000,         //服务端口号
    mongodb_url: 'mongodb://lwjserver:lwjserver123@localhost:27017/test-myapp',    //数据库地址
    prefix_url:'/api', //接口前缀
    redis_url:'',       //redis地址
    redis_port: '',      //redis端口号
    SECRET_KEY:'test-myapp',//token key
    TOKEN_TIME: "7d",//token过期时间
}