const router = require('koa-router')()
const Tag = require('../controller/tag');
router.prefix(`/api/tag`)

router.get('/',Tag.getAll);
router.post('/',Tag.createOne)
router.post('/delete',Tag.deleteOne)
module.exports = router
