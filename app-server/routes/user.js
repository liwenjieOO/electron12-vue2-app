const router = require('koa-router')()
const User = require('../controller/user');
router.prefix(`/api`)

router.post('/user/register',User.register);
router.post('/user/login',User.login)
router.post('/user/delete',User.deleteOne)
router.post('/user/update',User.updateOne)

module.exports = router
