const router = require('koa-router')()
const Cmd = require('../controller/cmd');
router.prefix(`/api/cmd`)

router.get('/',Cmd.getAll);
router.get('/item',Cmd.getOne);
router.post('/',Cmd.createOne)
router.post('/delete',Cmd.deleteOne)
router.post('/update',Cmd.updateOne)

module.exports = router
