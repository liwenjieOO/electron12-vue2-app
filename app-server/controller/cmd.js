const moment = require('moment')
const Cmd = require('../db/models/cmd')
const User = require('../db/models/user')
// const Increase = require('../db/models/increase');
const { SuccessRes, ErrorRes } = require('../utils/res');

exports.getAll = async ctx => {
  const { limit = 20, offset = 0, title, author } = ctx.query
  const filter = {}

  // if (tag) filter.tagList = tag

  // if (favorited) filter.favoritedList = favorited

  if (author) {
    const authorUser = await User.findOne({ username: author })
    if (authorUser) {
      filter.username = authorUser._id
      return ctx.body = new SuccessRes({})
    } else {
      return ctx.body = new ErrorRes({ code:401, msg: `${author}不存在` })
    }

  }
  if(title){
    filter.title = title;
  }

  const findResults = await Cmd.find(filter)
    .skip(Number(offset))
    .limit(Number(limit))
    .sort({ creatAt: -1 })

  // const cmds = []
  // for (let article of findResults) {
  //     const author = await User.findById(article.author)
  //     let following = false

  //     if (ctx.user) {
  //         following = author.assertFollower(ctx.user.id)
  //     }

  //     const target = {
  //         slug: article.slug,
  //         title: article.title,
  //         description: article.description,
  //         body: article.body,
  //         tagList: article.tagList,
  //         createdAt: article.createdAt,
  //         updatedAt: article.updatedAt,
  //         favorited: ctx.user ? article.assertFavorite(ctx.user.id) : false,
  //         favoritesCount: article.getFavoriteCount(),
  //         author: {
  //             username: author.username,
  //             bio: author.bio,
  //             image: author.image,
  //             following
  //         },
  //     }
  //     cmds.push(target)
  // }

  const articleCount = findResults.length;
  return ctx.body = new SuccessRes({ result: findResults, total: articleCount })
}

exports.updateOne = async (ctx) => {
  let { title, path, category, id } = ctx.request.body;
  let one = await Cmd.find({ _id: id })
  if (one.length > 0) {
    await Cmd.updateOne({ _id: id }, { $set: { title, path, category } })
    let result = Object.assign(one[0], { title, path, category })
    return ctx.body = new SuccessRes({ result })
  } else {
    return ctx.body = new ErrorRes({ code:401, msg: ',该命令不存在!' })
  }
}


exports.getOne = async (ctx) => {
  console.log('ctx0', ctx.request)
  let { id } = ctx.request.query;
  let result = await Cmd.find({ _id: id })
  console.log('result---', result, id)
  if (result.length > 0) {
    return ctx.body = new SuccessRes({ result })
  } else {
    return ctx.body = new ErrorRes({ code:401, msg: ',该命令不存在!' })
  }
}
exports.createOne = async (ctx) => {
  let { title, path, category } = ctx.request.body;
  let cmd = { title, path, category };
  if (!title || !path) {
    ctx.body = new ErrorRes({ code:401, message: '必填选项不能为空!' })
  } else {
    let one = await Cmd.find({ title: cmd.title });
    if (one.length == 0) {
      let created = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
      let New = Object.assign(cmd, { created });
      let result = await Cmd.create(New);
      return ctx.body = new SuccessRes({ result, msg: '创建成功' })
    } else {
      return ctx.body = new ErrorRes({ code:401, msg: ',已存在相同的cmd命令!' })
    }
  }
}

exports.deleteOne = async (ctx) => {
  let { id } = ctx.request.body;
  let cmd = await Cmd.findOne({ _id: id });
  if (!cmd) {
    return ctx.body = new ErrorRes({ code:401, msg: ',没有找到该命令!' })
  }
  let result = await Cmd.deleteOne({ _id: id });
  if (result) {
    result.msg = `删除名为${cmd.title}的命令`
    return ctx.body = new SuccessRes({ result })
  } else {
    return ctx.body = new ErrorRes({ code:401, msg: ',删除失败!' })
  }
}
